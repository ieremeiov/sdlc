package com.ieremeiov.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.ieremeiov.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());

	@Autowired
	// connection information to ActiveMQ and destination RME Queue
	JmsTemplate jmsTemplate;

	// to send JSON to Mongo
	@Autowired
	ConsumerAdapter consumerAdapter;

	public void onMessage(Message message) {
		// Message will come through as a JSON String

		logger.info("logging: onMEssage()");

		String json = null;

		if (message instanceof TextMessage) {

			try {

				json = ((TextMessage) message).getText();
				logger.info("Sending JSON to DB: " + json);
				consumerAdapter.sendToMongo(json);

			} catch (JMSException e) {
				// send to RME queue
				jmsTemplate.convertAndSend(json);
				logger.error("Message: " + json);
			} catch (UnknownHostException e) {
				// send to RME queue
				jmsTemplate.convertAndSend(json);
				logger.error("Message: " + json);
			} catch (Exception e) {
				// send to RME queue
				jmsTemplate.convertAndSend(json);
				logger.error("Message: " + json);
			}
		}

	}

}
