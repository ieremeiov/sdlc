package com.ieremeiov.jms.adapter;

import java.net.UnknownHostException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.ieremeiov.jms.listener.ConsumerListener;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());

	public void sendToMongo(String json) throws UnknownHostException {

		logger.info("Sending to MongoDB");

		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));

		// creates the db if there isn't one
		DB db = client.getDB("vendor");

		// table
		DBCollection collection = db.getCollection("contact");

		logger.info("Convert JSON to DB Object");

		// --------------------------------
		// PRINT ALL OBJECTS IN DB
		printCollection(collection);

		// --------------------------------

		DBObject object = (DBObject) JSON.parse(json);
		collection.insert(object);

		logger.info("Sent to MongoDB");

	}

	private static void printCollection(DBCollection collection) {
		DBCursor curs = collection.find();
		if (curs.hasNext()) {
			DBObject o = curs.next();
			System.out.println(o.toString());
		}
	}
}
