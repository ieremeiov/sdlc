package com.ieremeiov.jms.producer.controller;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ieremeiov.jms.producer.model.Vendor;
import com.ieremeiov.jms.producer.service.MessageService;

@Controller
public class ProducerController {

	private static Logger logger = LogManager.getLogger(ProducerController.class);

	@Autowired
	private MessageService messageService;

	@RequestMapping("/")
	public String renderVendorPage(Vendor vendor, Model model) {
		logger.info("Rendering Index JSP");
		return "index";
	}

	@RequestMapping(value = "/vendor", method = RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("vendor") Vendor vendor, Model model) {

		logger.info("Processing Vendor Object");
		logger.info(vendor.toString());

		// Process the Vendor Information using the Service
		messageService.process(vendor);

		ModelAndView mv = new ModelAndView();
		mv.addObject("message", "Message Added Successfully");

		// clear the vendor values, overwrite model with empty vendor
		vendor = new Vendor();
		mv.addObject("vendor", vendor);

		mv.setViewName("index");

		return mv;
	}

}
