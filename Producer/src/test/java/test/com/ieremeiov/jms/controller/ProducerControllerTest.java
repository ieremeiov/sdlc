package test.com.ieremeiov.jms.controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.ieremeiov.jms.producer.controller.ProducerController;
import com.ieremeiov.jms.producer.model.Vendor;

public class ProducerControllerTest {

	private Vendor vendor;

	private Model model;

	private ProducerController producerController;

	private ApplicationContext context;

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		
		vendor = new Vendor();
		vendor.setAddress("123 Main st");
		vendor.setCity("maintown");
		vendor.setEmail("bob.ups.com");
		vendor.setFirstName("Bob");
		vendor.setLastName("Thomas");
		vendor.setPhoneNumber("123-456-7890");
		vendor.setState("iowa");
		vendor.setVendorName("UPS");
		vendor.setZipCode("12345");
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext) context).close();
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
